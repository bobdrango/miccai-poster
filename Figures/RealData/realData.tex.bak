\begin{figure}[tbp]

\setlength{\iwidth}{0.15\textheight}
\setlength{\iheight}{1\iwidth}
%\setlength{\tabcolsep}{-1pt}
\renewcommand\arraystretch{0.2}

\newcommand{\ratio}{2}


%% Color bar based on Mango's 'spectrum' color table
%%\newlength\colorbarlength
%%\setlength{\colorbarlength}{\iwidth}
%\setlength{\colorbarlength}{1.4in}
%\pgfdeclarehorizontalshading{spectrum}{0.08\colorbarlength}
%{rgb(0)=(0,0,0); rgb(0.10\colorbarlength)=(0,0,1); rgb(0.33\colorbarlength)=(0,1,1);
%rgb(0.5\colorbarlength)=(0,1,0); rgb(0.66\colorbarlength)=(1,1,0); rgb(0.90\colorbarlength)=(1,0,0);
%rgb(1.0\colorbarlength)=(1,1,1)}

\centering
\vspace{8pt}
\hspace{-11pt}

\def \filePath {Figures/realData}
\begin{tabular}{ccc}
	
\textbf{Ground Truth} & 
\textbf{Simple Averaging} & 
\textbf{Proposed}
\\[4pt]
%[-10pt]

\begin{tikzpicture}[baseline]
\draw (0,0)[black, ultra thick, fill=black] rectangle(\iwidth,\iheight);

\begin{pgfonlayer}{foreground}
\clip (0,0) rectangle (\iwidth,\iheight);
\node at (0.5\iwidth,0.5\iheight) {
	\includegraphics[width=0.39\ratio\iwidth]{\filePath/groundTruthLine.png}
};
%\node[fancylabel] at (0.02\iwidth,0.2\iheight) {A};
\end{pgfonlayer}
%\draw[thin,Blue1] (0.50\iwidth,0.32\iheight) rectangle ++(0.16\iwidth,0.16\iwidth);

\end{tikzpicture}

&

\begin{tikzpicture}[baseline]
	\draw (0,0)[black, ultra thick, fill=black] rectangle(\iwidth,\iheight);

  \begin{pgfonlayer}{foreground}
  \clip (0,0) rectangle (\iwidth,\iheight);
    \node at (0.5\iwidth,0.5\iheight) {
    \includegraphics[width=0.39\ratio\iwidth]{\filePath/MeanLine45d9s.png}
    };
%    \node[fancylabel] at (0.02\iwidth,0.2\iheight) {B};
  \end{pgfonlayer}
%\draw[thin,Blue1] (0.50\iwidth,0.32\iheight) rectangle ++(0.16\iwidth,0.16\iwidth);

\end{tikzpicture}

&

\begin{tikzpicture}[baseline]
	\draw (0,0)[black, ultra thick, fill=black] rectangle(\iwidth,\iheight);

  \begin{pgfonlayer}{foreground}
  \clip (0,0) rectangle (\iwidth,\iheight);
    \node at (0.5\iwidth,0.5\iheight) {
    \includegraphics[width=0.39\ratio\iwidth]{\filePath/AtlasLine45d9s.png}
    };
%  \node[fancylabel] at (0.02\iwidth,0.2\iheight) {C};
  \end{pgfonlayer}
%  \draw[thin,Blue1] (0.50\iwidth,0.32\iheight) rectangle ++(0.16\iwidth,0.16\iwidth);

\end{tikzpicture}

\\
\\

\begin{tikzpicture}[baseline]
	\draw (0,0)[black, ultra thick, fill=black] rectangle(\iwidth,\iheight);

  \begin{pgfonlayer}{foreground}
  \clip (0,0) rectangle (\iwidth,\iheight);
    \node at (0.5\iwidth,0.5\iheight) {
    \includegraphics[width=0.9999\ratio\iwidth]{\filePath/groundTruthCross.png}
    };
%  \node[fancylabel] at (0.02\iwidth,0.2\iheight) {D};
  \end{pgfonlayer}
%  \draw[thin,Blue1] (0.50\iwidth,0.32\iheight) rectangle ++(0.16\iwidth,0.16\iwidth);

\end{tikzpicture}

&

\begin{tikzpicture}[baseline]
	\draw (0,0)[black, ultra thick, fill=black] rectangle(\iwidth,\iheight);

  \begin{pgfonlayer}{foreground}
  \clip (0,0) rectangle (\iwidth,\iheight);
    \node at (0.5\iwidth,0.5\iheight) {
    \includegraphics[width=0.9999\ratio\iwidth]{\filePath/MeanCross15d5s.png}
    };
%  \node[fancylabel] at (0.02\iwidth,0.2\iheight) {E};
  \end{pgfonlayer}
%  \draw[thin,Blue1] (0.50\iwidth,0.32\iheight) rectangle ++(0.16\iwidth,0.16\iwidth);

\end{tikzpicture}

&

\begin{tikzpicture}[baseline]
	\draw (0,0)[black, ultra thick, fill=black] rectangle(\iwidth,\iheight);

  \begin{pgfonlayer}{foreground}
  \clip (0,0) rectangle (\iwidth,\iheight);
    \node at (0.5\iwidth,0.5\iheight) {
    \includegraphics[width=0.9999\ratio\iwidth]{\filePath/AtlasCross15d5s.png}
    };
%  \node[fancylabel] at (0.02\iwidth,0.2\iheight) {F};
  \end{pgfonlayer}
%  \draw[thin,Blue1] (0.50\iwidth,0.32\iheight) rectangle ++(0.16\iwidth,0.16\iwidth);

\end{tikzpicture}
	
\end{tabular}

\caption{
	Comparison of fiber ODFs. (Left) Ground-truth ODFs. (Middle) ODFs given by simple averaging. (Right) ODFs given by the proposed method. The results were generated using synthetic dataset with $9\%$ noise and $\theta_{\text{T}}=45^{\circ}$ and $5\%$ noise and $\theta_{\text{T}}=15^{\circ}$ respectively for the one- and two-direction cases.
	\label{fig:SyntheticDataVIsualExamples}
}
\label{fig:syntheticODF}
\end{figure}

